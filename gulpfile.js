﻿var gulp = require('gulp');
var del = require('del');
var debug = require('gulp-debug');
var concat = require('gulp-concat');
var minifyCss = require('gulp-clean-css');
var inlineHtml = require('gulp-inline-source');
var minifyHtml = require('gulp-htmlmin');

/* -------------------------------- */

gulp.task('buildImages', function() {
    return gulp.src('src/images/**')
        .pipe(debug())
        .pipe(gulp.dest('wwwroot/images'));
});

/* -------------------------------- */

gulp.task('cleanCss', function (done) {
    del([
            'tmp/concatcss',
            'tmp/minifycss'
        ]);
    done();
});

gulp.task('concatCss', function () {
    return gulp.src('src/styles/*.css')
            .pipe(debug())
            .pipe(concat('styles.css'))
            .pipe(gulp.dest('tmp/concatcss'));
});

gulp.task('minifyCss', gulp.series('concatCss', function () {
    return gulp.src('tmp/concatcss/styles.css')
            .pipe(debug())
            .pipe(minifyCss({ debug: true }, function (details) {
                console.log(details.name + ' - original size: ' + details.stats.originalSize + ', final size: ' + details.stats.minifiedSize);
            }))
            .pipe(gulp.dest('tmp/minifycss'));
}));

gulp.task('buildCss', gulp.series('minifyCss', function() {
    return gulp.src('tmp/minifycss/styles.css')
        .pipe(debug())
        .pipe(gulp.dest('wwwroot/generated'));
}));

/* -------------------------------- */

/* -------------------------------- */

gulp.task('cleanHtml', function (done) {
    del([
        'tmp/inlinehtml',
        'tmp/minifyhtml',
        'wwwroot/*.html'
        ]);
        done();
});

gulp.task('inlineHtml', gulp.series('minifyCss', function () {
    return gulp.src('src/html/*.html')
            .pipe(debug())
            .pipe(inlineHtml())
            .pipe(gulp.dest('tmp/inlinehtml'));
}));

gulp.task('minifyHtml', gulp.series('inlineHtml', function () {
    return gulp.src('tmp/inlinehtml/*.html')
            .pipe(debug())
            .pipe(minifyHtml({collapseWhitespace: true}))
            .pipe(gulp.dest('tmp/minifyhtml'));
}));

gulp.task('buildAll', gulp.series('buildCss', 'minifyHtml', function () {
    return gulp.src('tmp/minifyhtml/*.html')
            .pipe(debug())
            .pipe(gulp.dest('wwwroot'));
}));

/* -------------------------------- */

gulp.task('cleanAll', gulp.series('cleanCss', 'cleanHtml', function (done) {
    del(['tmp']);
    done();
}));
